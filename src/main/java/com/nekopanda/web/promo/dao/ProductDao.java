package com.nekopanda.web.promo.dao;

import com.nekopanda.web.promo.entity.Product;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ProductDao extends PagingAndSortingRepository<Product,String> {
}

package com.nekopanda.web.promo.controller;

import com.nekopanda.web.promo.dao.ProductDao;
import com.nekopanda.web.promo.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@RestController
public class ProductController {
    @Autowired
    private ProductDao productDao;

    @GetMapping("/product/")
    public Iterable<Product> findAllProduct(){
        return productDao.findAll();
    }

    @GetMapping("/hostinfo")
    public Map<String,Object> backendInfo(HttpServletRequest request){
        Map<String, Object> maps = new HashMap<>();
        maps.put("hostname",request.getLocalName());
        maps.put("ip",request.getLocalAddr());
        maps.put("port",request.getLocalPort());
        return maps;
    }
}
